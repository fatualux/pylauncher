import os
import webbrowser
from flask import Flask, render_template, request, jsonify, abort
import subprocess
import json
from incremental_search import incremental_search

pylauncher = Flask(__name__)

json_file = './pylauncher.json'


# Function to open browser on server startup
def startup_server(address, port):
    if os.name == 'nt':
        address = '127.0.0.1'
    webbrowser.open(f"http://{address}:{port}")


startup_server('0.0.0.0', 5000)


@pylauncher.route('/')
def index():
    return render_template('search.html')


@pylauncher.route('/search', methods=['POST'])
def search():
    if not request.is_json:
        abort(400, 'Request must be JSON')

    query = request.json.get('query', '').strip()  # Trim whitespace

    if not query:
        # If the query is empty, return all items from the JSON file
        with open(json_file, 'r') as file:
            pylauncher = json.load(file)
        return jsonify(pylauncher)

    # Perform incremental search
    results = incremental_search(json_file, query)

    return jsonify(results)


@pylauncher.route('/run-command', methods=['POST'])
def run_command():
    command = request.json.get('command', '')
    if not command:
        return jsonify({'error': 'Command not provided'})
    try:
        print("Received command:", command)  # Debugging statement
        # Run the command and capture the output
        result = subprocess.run(command, shell=True,
                                capture_output=True, text=True)
        command_output = result.stdout
        print("Command output:", command_output)  # Debugging statement
        # Render the result template with the command output
        return render_template('result.html', output=command_output)
    except Exception as e:
        return jsonify({'error': str(e)})


if __name__ == '__main__':
    pylauncher.run()
