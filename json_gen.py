import json

JSON_FILE = "pylauncher.json"

# Initialize an empty list to store data
data = []

# For loop to ask user for name and command
for i in range(1, 5):
    name = input(f"Enter the name for element {i} (type 'exit' to finish): ")
    if name.lower() == 'exit':
        break
    command = input(f"Enter the command for element {i}: ")
    data.append({"name": name, "command": command})

# Convert data to JSON format
json_data = json.dumps(data, indent=2)

# Write JSON data to file
with open(JSON_FILE, 'w') as f:
    f.write(json_data)

print(f"JSON data has been written to {JSON_FILE}")
