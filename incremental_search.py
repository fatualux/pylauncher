import json

def incremental_search(json_file, partial_query):
    with open(json_file, 'r') as file:
        data = json.load(file)

    results = []

    for item in data:
        # Perform case-insensitive search
        if partial_query.lower() in item['name'].lower():
            results.append(item)

    return results
