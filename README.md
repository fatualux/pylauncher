# PYLAUNCHER

### This is a simple launcher written in Python. <br />

## REQUIREMENTS

[![Python](https://img.shields.io/badge/Python-3.7-blue.svg)](https://www.python.org/downloads/) or higher

## INSTALLATION

### Running the app within a Python virtual environment

Note: for this procedure to work, you need to have ***virtualenv*** installed.

```
git clone https://www.gitlab.com:fatualux/pylauncher && cd pylauncher
sh ./pylauncher.sh
```

### Running the app outside a virtual environment

```
git clone https://www.gitlab.com:fatualux/pylauncher && cd pylauncher
```

You can install all the needed dependencies with the following command:

```
python -m pip install -r requirements.txt
```

## USAGE

```
python pylauncher.py
```

Then, open your preferred browser and go to http://127.0.0.1:5000/

Done!

## LICENSE

[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

This project is licensed under the GPLv3 license.
See LICENSE file for more details.
